-- LuaScript1
-- Author: Magnusm
-- DateCreated: 2/19/2019 8:30:37 PM
--------------------------------------------------------------
changelog = nil
map = {}
lastTurn = 0
print("Loading End of Game Replay (EoGR) 5")

function SaveTable(table,slot)
	ExposedMembers.SaveTableToSlot(table,slot)
end

function LoadTable(slot)
	return ExposedMembers.LoadTableFromSlot(slot)
end

function TryLoadData()
	changelog = LoadTable("EoGR_TileState")
	if(changelog == nil) then
		print("No data to be found. Starting with clean databank")
		changelog = {}
		SaveTable(t,"EoGR_TileState")
	else
		print("Data loaded!")
	end
end

function arrayLength(a)
	local d = 0
	for i in pairs(a) do
		d = d + 1
	end
	return d
end

function processMap()	
	w,h = Map.GetGridSize()
	local turn = Game.GetCurrentGameTurn()
	local changes = {}
	for i = 0,(w-1) do 
		for j=0,(h-1) do 
			local ind = (i*h) + j
			local owner = Map.GetPlot(i,j):GetOwner()
			if(map[ind] ~= owner) then
				map[ind] = owner
				table.insert(changes,ind)
				table.insert(changes,owner)
			end
		end 
	end  
	local count = arrayLength(changes)
	if count > 0 then
		table.insert(changelog,turn)
		table.insert(changelog,count)
		for k,v in pairs(changes) do
			table.insert(changelog,v)
		end
		SaveTable(changelog,"EoGR_TileState")
	end
	print((count/2) .. " Plots changed ownership this turn.")
end


function StartOfTurn()
	if(changelog == nil) then
		print("No data found yet. First turn?")
		TryLoadData()
	end
	processMap()
end


function initialize()
	changelog = nil
	w,h = Map.GetGridSize()
	for i = 0,(w-1) do 
		for j=0,(h-1) do 
			local ind = (i*h) + j
			map[ind] = Map.GetPlot(i,j):GetOwner()
		end 
	end  
end

initialize()
Events.TurnBegin.Add(StartOfTurn)
print("EoGR Initialized")